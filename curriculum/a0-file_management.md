File Management
===============

Everything here is a file!


## Basic Operations

Now, for each time you run command, check your directory (with 'ls')

Copy a file

    cp /etc/debian_version .


You can do it with different name too

    cp /etc/debian_version my_version

Or

    cp /etc/debian_version ./my_version

Note that the current directory (denoted with '.') is mentioned implicitly. This is called relative addressing (location relative to current).


Now let's use the absolute path

    cp my_version /tmp
    cd /tmp

Make a new directory

    mkdir my_dir

Move your file to your directory

    mv my_version my_dir

To move your file back

    mv my_dir/my_version .

Or

    cd my_dir
    mv my_version ..
    cd ..

With the same command, you can rename files

    mv my_version your_version

Move back to your home directory

    cd ~

Or

    cd

Now remove the file you copied the first time

    rm your_version

Now try it with the directory you created earlier

    rm /tmp/my_dir

Nope, regular remove won't work on directories (for safety reason)

    rmdir /tmp/my_dir

There you go

Let's copy a command we've been using; it is located in a system directory

    cp /bin/ls .

By the way, this is how it is with all the commands we've used so far!

To execute your newly copied program on the current directory

    ./ls

You can try it with cp too; also try with arguments!

To list files with details

    ls -l


## Permission & Ownership

Look at the output of the previous 'ls -l' command
* Access bit: left-end column
* Inode: second column--we'll discuss this later
* Owner: the next two columns--respectively user, then group

View the name of your user (and group)

    id

Try it with options!

Try one of these and check again (with the 'ls' command)

    chown <your_user>:<your_group> ls
    chmod 700 ls

What do you see?

Try executing the copied 'ls'.

Remove the *execute* bit from the file permission

    chmod a-x ls

Try executing file again.


## File Sizes

Look at the output again, watch the number on the middle column

Try (notice the **-h**):

    ls -l -h

Observe the ls file you just copied

We are now going to make this file blank.

    echo '' > ls

Look again!

Notice that the actual size of directories are not displayed on the output. To view the total size of directory content:

    du -sh [dir]

Omitting [dir] will yield to current dir

This recursively counts the size of all the content under the specified directory


## Timestamp

Look at the output of 'ls -l' again, and this time, watch the last columns before the file name.

Timestamp of a file gets updated when the file gets used.

Change the timestamp:

    touch ls

Look again!


## Links

Filesystem doesn't actually care about file paths; all it got has a vast space to put things. While most of this space is free for all, a small chunk is reserved for bookkeeping. It is done by indexing the space used, with each file occupying an entry called inode.

Everytime a file is created, some space gets allocated, new inode is created, a file path gets linked to it. Inodes can be linked to more than one paths, kept track with a number.

The number increases when path gets linked (with hard link), and decreases when one gets unlinked to it (with file deletion).

Two types of link:
* Hardlink -- this one creates a new directory entry and link it directly to an **inode**; most people don't use this as it may cause complications.
* Softlink -- this one simply creates a new file (in a new inode) containing a symbol that points to the target **path**; also called symbolic link (symlink), use this one!

Try it!

    cat /etc/debian_version
    ln -s /etc/debian_version .

Symlinks are accessed like regular file.

    ls debian_version
    cat debian_version

You can see links through ls command (light blue if valid, blinking red if invalid).

Now remove the link (like regular file)!

Beware the 'deletion' you know of doesn't actually remove the data until the space is needed, in which case it's simply overwritten. For sensitive data, you'd want to use special technique, in which the old data immediately gets overwritten.


## File Types

File name extension does not always denote file type; they're just part of the name. In fact, often times programs disregard extension entirely. On the other hand, files always come with metadata, which contains a 'magic number'.

To view type of file

    file <file_name>

Try it!

    file /bin/ls
    file /etc/debian_version
    file /dev/sda
