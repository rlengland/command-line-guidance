Super Usage
===========

With power comes responsibility


## Virtualization

We're going to do everything here in a virtual machine. That way you won't permanently break the current system. Virtual machines tend to load their files on virtual filesystems. This is what we're going to use.

Now look for the virtual filesystem we're going to use, which contains a live-bootable system. A live-bootable system is a system that can be booted from external drive. Generally, they won't allow permanent changes.

Let's assume it is on 'image.iso'.

    qemu-system-x86_64 -m 1G -enable-kvm -hda image.iso

A bit of explanation:
* the command 'qemu-system-x86_64' initiates a virtual machine
* the option '-m 1G' allocates 1 GB of RAM for the virtual machine
* the option '-hda image.iso' is for the location of disk image

As the terminal gets unresponsive, you should see a new window, showing what looks like a miniature computer screen with 'QEMU' showing on its title bar. We'll call this our "guest system", while the system it's running on would be "host system".

You may ignore warnings that would show on the terminal as long as the new window appears. Some OS might not support the KVM feature; if so, omit '-enable-kvm' option.

Everything here from this point should be done in the QEMU screen.

You should now see another terminal; you may notice it looks slightly different. You can resize the window (with mouse) to make it fit your screen. You may need to follow the prompt. Press Alt+LeftArrow or Alt+RightArrow to move between terminals.


## Package Manager

Your programs come in packages, and your system would have to manages these. If you're familiar with Windows, you may have to install program at some point. You can also install program on your Linux system as well.

You would most likely have APT based system. There are others, such as pacman and RPM, but we won't go over that.

Consider this package you don't have: neofetch -- a simple information display utility.

Now type:

    neofetch

You will notice a "command not found" error, because you don't have the program in question

To fix that

    apt install neofetch

You may have to answer interactive prompt.

Your package database might be outdated. If so, update it

    apt update

Then you can try to install the package again. Once installed, run it!

    neofetch

You can view the list of installed packages

    apt list --installed

You can safely ignore what comes out.


## System Daemons

Let's consider this daemon: GPM, for a program that enables mouse on terminal. If you hover your mouse to the QEMU screen, you would see a (rather primitive) mouse pointer.

To view the daemon status

    systemctl status gpm

To stop the daemon

    systemctl stop gpm

Check the status again!

Finally, to start the daemon

    systemctl start gpm

You can also enable or disable a daemon, but it will have no effect on a live-booted systems, since they don't keep any permanent change to their files.


## System Settings

Configuration of the system setting has to be stored somehow. Generally the settings are stored under '/etc' directory. User settings are stored under their own home directory.

Let us look at an example: vim is a command line text editor; it stores its global settings under '/etc/vim/' directory.

Now try and edit it

    vim /etc/vim/vimrc

You can use the arrow keys to navigate your cursor. To start editing, press 'i' for insert mode. To enter the vim console, type colon (':'), followed by a command, and finally an Enter. Vim console is **not** the same as your terminal!

Go to the last line of file, and add a line containing

    set number

To save changes, type 'write' on the console, or simply 'w'. Finally, type 'quit', or simply 'q' to get out. REMEMBER THIS!

After exiting, try running vim again. Notice any difference?


## Log Files

The system keeps track runtime record. You can tell a lot by looking your system log.

Now type:

    journalctl

or

    journalctl -eb

Don't read everything here -- we don't have all day!


## Switching User

Your system may be given access to different people. In that case, you may want different level of access. This is why we have ownership and permission.

To create user (named 'person')

    useradd -m person

To switch to user

    su - person

Other systems may instead use sudo, which comes with more features (and hence more complicated). We won't be covering this.

Type

    ls /root

Notice the 'permission denied' error. This is because the directory you're accessing is restricted to root.

    ls -d -l /root

To quit, press Ctrl+D; otherwise, type 'exit' or 'logout'.

Try accessing '/root' again (as root). If you try typing 'pwd', you will see that this is actually the root's home directory. Do **not** confuse this with the root filesystem itself ('/').

Another name for root would be 'super user' -- you can do a lot (of damage) with it.


## File Access

Create a new directory on root filesystem as the user 'person'

    mkdir /new_dir

Notice the permission error. This is because regular users aren't normally given write access to the root filesystem.

    ls -d -l /

Try creating the directory again as root.

Now as the user 'person', create a new file on the new directory

    touch /new_dir/new_file

Permission error, again. So as root

    chmod 1777 /new_dir

or

    chown person:person /new_dir

Try creating the file once again.

Finally, as root, type this command

    rm /bin/rm

You just removed your remove command. Now you can't remove anything (try it)!
