For Developers
==============

Markdown guide [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) and [there](https://www.markdownguide.org/cheat-sheet)

You can generate PDF from Markdown using Atom IDE with markdown-themeable-pdf extension.

The header styles are chosen for following consideration
- h1 uses underline for visibility
- h2 uses hashtag for parsing

To generate table of content (thru h2) from <file.md>

    grep '^## ' <file.md> | sed 's/^##/-/'

This finds lines that begins with two hashtags and a space, then replaces the two hashtags with a dash.
