Platform-specific Information
=============================

- liveCD (.iso file) is much simpler to use -- no decompression necessary, read-only by default!

- for now we use grml, a Linux distro for system administration with no graphical interface

- that would be grml64-small_2018.12.iso

* on grml boot, press Q to exit from setup screen to regular terminal

* update is recorded to take 30-50 seconds; this uses apt
