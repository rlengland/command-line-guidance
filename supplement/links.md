Useful Links
============


[Original Google Drive Folder](https://drive.google.com/drive/folders/0B8vcsfre4ynkRWRuRDhhTmVWT3c)  
Contains:
- official facilitator guides
- rough draft
- old notes

[A Markdown guide](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) and [Another Markdown guide](https://www.markdownguide.org/cheat-sheet)  
Good for getting started if you plan on modifying anything here.
